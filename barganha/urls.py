"""barganha URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
from app import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static
from .import settings
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('index', views.index),
    path('logar', views.logar),
    path('submit', views.submit),
    path('logout', views.logout_user),
    path('novo', views.novo),
    path('registrar', views.registrar),
    path('list_user', views.list_user),
    path('obejeto/<id>/', views.obejeto),
    path('obejeto/<id>/logout', views.logout_user_id),
    path('obejeto/<id>/index', views.index_id),
    path('obejeto/<id>/list_user', views.list_user_id),
    path('obejeto/<id>/valor', views.valor),
    path('obejeto/<id>/delete/', views.delete),
    path('new_obejet', views.new_obejet),
    path('postar', views.postar),
    path('pesquisar', views.pesquisar),
    path('Tecnologia', views.tecnologia),
    path('casa', views.casa),
    path('lazer', views.lazer),
    path('livro', views.livro),
    path('obejeto/<id>/proposta/', views.proposta),
    path('obejeto/<id1>/proposta/obejeto/<id2>', views.aceita),
    path('propostas', views.propostas),
    path('barganha/<id>', views.barganha),
    path('barganha/index/', views.inicio),
    path('barganha/list_user/', views.list),
    path('barganha/logout/',views.sair),
    path('deletar/<id>/', views.deletar),
    path('obejeto/<id>/proposta/index/', views.index_id),
    path('obejeto/<id>/proposta/list_user', views.list_user_id),
    path('obejeto/<id>/proposta/logout', views.logout_user_id),
    path('conversa/<id>', views.conversa),
    path('conversa/conversa/<id>',views.conversa),

]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)