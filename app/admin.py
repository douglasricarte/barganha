from django.contrib import admin
from .models import post, categoria, negocio

# Register your models here.
admin.site.register(post)
admin.site.register(categoria)
admin.site.register(negocio)