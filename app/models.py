from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class categoria(models.Model):
    nome = models.CharField(max_length=100)

    def __str__(self):
        return self.nome
class post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    nome = models.CharField(max_length=20)
    sobre = models.TextField()
    imagem = models.ImageField(upload_to='post', blank=True, null=True)
    estado = models.CharField(max_length=100)
    cidade = models.CharField(max_length=100)
    valor = models.IntegerField()
    ativo = models.BooleanField(null=True, default=True)
    categoria = models.CharField(max_length=50, null=True)
    fone = models.CharField(max_length=80, null=True)
    def __str__(self):
            return self.nome
class negocio(models.Model):
    id1 = models.IntegerField()
    id2 = models.IntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    id_user = models.IntegerField(null=True)
    data = models.DateField(auto_now_add=True, null=True)
    conversa = models.TextField(null=True, default="Comece a barganhar :)")
    nome1 = models.CharField(max_length=50, null=True)
    nome2 = models.CharField(max_length=50, null=True)
    