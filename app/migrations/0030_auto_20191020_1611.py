# Generated by Django 2.2.5 on 2019-10-20 19:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0029_remove_negocio_user2'),
    ]

    operations = [
        migrations.AddField(
            model_name='negocio',
            name='id_user',
            field=models.IntegerField(null=True),
        ),
        migrations.DeleteModel(
            name='user2',
        ),
    ]
