from django.shortcuts import render
# Create your views here.
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, logout, login
from django.contrib import messages
from django.template.defaultfilters import upper
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.decorators import login_required
from .models import post, negocio
import os
import html
from django.core import serializers

# Create your views here.

def index(request):
    posta = post.objects.filter(ativo=True)
    return render(request, 'index.html',{'post': posta})
@csrf_protect
def logar(request):
    return render(request, 'login.html')
@csrf_protect
def submit(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user, backend=None)
            return redirect(index)
        else:
            messages.error(request, " Usuario ou Senha invalida, tente novamente!")
            return redirect(logar)
    else:
        return render(request, 'index.html')
@login_required(login_url='/logar')
def logout_user(request):
    logout(request)
    return redirect(logar)
def novo(request):
    try:
        if request.POST:
            user = User.objects.create_user(username=request.POST.get('nome'),email=request.POST.get('email'),password=request.POST.get('senha'))
        user.save()
    except:
        messages.error(request, " Nome de usuario já está sendo utilizado!")
        return redirect(registrar)
    return redirect(logar)
def registrar(request):
    return render(request, 'registro.html')
@login_required(login_url='/login/')
def list_user(request):
    posta = post.objects.filter(user=request.user)
    return render(request, 'index2.html',{'post': posta})
@login_required(login_url='/logar')
def obejeto(request,id):
    obejeto = post.objects.get(id=id)
    return render(request, 'obejeto.html',{'obejeto': obejeto})
@login_required(login_url='/logar')
def index_id(request, id):
    return redirect(index)
@csrf_protect
def logout_user_id(requeste, id):
    return redirect(logout_user)
@login_required(login_url='/logar')
def list_user_id(request,id):
    return redirect(list_user)
@csrf_protect
@login_required(login_url='/logar')
def valor(request, id):
    obejeto = post.objects.get(id=id)
    aux = obejeto.valor + (obejeto.valor * 0.20)
    aux1 = obejeto.valor - (obejeto.valor * 0.20)
    aux2 = float (request.POST.get('valor'))
    if(aux2 <= aux and aux2 >= aux1):
        obejeto.valor = (obejeto.valor + aux2)/2
        obejeto.save()
    return render(request, 'obejeto.html', {'obejeto': obejeto})
@login_required(login_url='/logar')
def delete(request,id):
    obejeto = post.objects.get(id=id)
    if obejeto.user == request.user:
        obejeto.delete()
        a = "media/" + str(obejeto.imagem)
        os.remove(a)
    return redirect(index)
@login_required(login_url='/logar')
def new_obejet(request):
    return render(request, 'post.html')
@login_required(login_url='/logar')
def postar(request):
    if request.POST:
        nome = request.POST.get('nome')
        sobre = request.POST.get('descricao')
        cidade = request.POST.get('cidade')
        estado = request.POST.get('estado')
        user = request.user
        file = request.FILES.get('file')
        valor = request.POST.get('valor')
        fone = request.POST.get('fone')
        postagem = post.objects.create(nome = nome, sobre = sobre, cidade= cidade, estado=estado,
                                       user = user, imagem =file, valor = valor, categoria = request.POST.get('select'), fone = fone)
        return redirect(index)
    return redirect(index)
@csrf_protect
def pesquisar(request):
    if request.POST:
        nome = post.objects.filter(ativo=True, nome=request.POST.get('pesquisar'))
        cidade = post.objects.filter( cidade = request.POST.get('pesquisar'))
        nome_estado = upper(str(request.POST.get('pesquisar')))
        estado = post.objects.filter( estado = nome_estado)

        return render(request, 'index2.html', {'post': nome | cidade | estado})
def tecnologia(request):
    posta = post.objects.filter(ativo=True, categoria = 'Tecnologia')
    return render(request, 'index2.html', {'post': posta})
def casa(request):
    posta = post.objects.filter(ativo=True, categoria = 'casa')
    return render(request, 'index2.html', {'post': posta})
def lazer(request):
    posta = post.objects.filter(ativo=True, categoria = 'Lazer')
    return render(request, 'index2.html', {'post': posta})
def livro(request):
    posta = post.objects.filter(ativo=True, categoria = 'Livro')
    return render(request, 'index2.html', {'post': posta})
@login_required(login_url='/logar')
def proposta(request,id):
    posta = post.objects.filter(user=request.user)
    proposta = post.objects.filter(id=id)
    return render(request, 'proposta.html',{'proposta':posta | proposta})
@login_required(login_url='/logar')
def aceita(request,id1,id2):
    posta = post.objects.filter(id= id1)
    user1 = User.objects.filter(username = request.user).values()
    proposta = post.objects.filter(id=id2)
    postar = post.objects.filter(id=id1).values()
    user2 = User.objects.filter(id =  postar[0]['user_id']).values()
    barganha = negocio.objects.create(id1 = id1, id2 =id2, user = request.user, id_user = postar[0]['user_id'], nome1 = user1[0]['username'], nome2= user2[0]['username'] )
    return redirect(index)
@login_required(login_url='/logar')
def propostas(request):
    lista = negocio.objects.filter(user = request.user).values()
    user2 = User.objects.filter(username = request.user).values()
    listauser = negocio.objects.filter(id_user = user2[0]['id']).values()
    return render(request, 'oferta.html', {'proposta': lista | listauser})
@login_required(login_url='/logar')
def barganha(request,id):
    lista = negocio.objects.filter(id = id).values()
    obejeto1 =  post.objects.filter(id = lista[0]['id1'])
    obejeto2 = post.objects.filter(id=lista[0]['id2'])
    return render(request, 'barganha.html', {'lista': obejeto1 | obejeto2})
@login_required(login_url='/logar')
def inicio(request):
    return redirect(index)
@login_required(login_url='/logar')
def list(request):
    return redirect(list_user)
def sair(request):
    return redirect(logout_user)
def deletar(request, id):
    barg = negocio.objects.filter(id =id)
    barg.delete()
    return redirect(index)
@csrf_protect
def conversa(request, id):
        if request.POST:
            objt = negocio.objects.get(id =id)
            lista = User.objects.filter(username=request.user).values()
            objt.conversa = objt.conversa +"\n" + str(lista[0]['username']) +": "  + str(request.POST.get('conversa'))
            objt.save()
            return redirect(propostas)
